#include <stdio.h>
#include <stdlib.h>

struct student
{
    char firstname[50];
    char subject[50];
    float marks;
};

int main()
{
    struct student s[5];
    int i;
    printf("Enter information of students :\n\n");

    //storing information
    for(i=0;i<5;i++)
    {
        printf("\nEnter first name :");
        scanf("%s",s[i].firstname);
        printf("Enter subject :");
        scanf("%s",s[i].subject);
        printf("Enter marks :");
        scanf("%f",&s[i].marks);
    }

    printf("Displaying information :\n\n");

    //Displaying Information
    for(i=0;i<5;i++)
    {
        printf("\nfirstname :%s\n",s[i].firstname);
        printf("subject :%s\n",s[i].subject);
        printf("marks :%.1f",s[i].marks);
        printf("\n");
    }


    return 0;
}
